
function empty(x) {
	if (x == undefined || x == NaN || x == null || x == '')
		return true;
	else
		return false;
};

var t13n=t13n||{};

// basic lipi details

t13n.scripts={};
t13n.scripts.list = ['deva', 'beng', 'guru', 'gujr', 'orya', 'taml', 'telu', 'knda', 'mlym'];
t13n.scripts.unicode = {start: 2304, end: 3456};

//	find script code from lang code
t13n.scripts.getScriptByLang = function(lang){
	if(t13n.alphabets.deva.langs.indexOf(lang)>=0) return 'deva';
	else if(t13n.alphabets.beng.langs.indexOf(lang)>=0) return 'beng';
	else if(t13n.alphabets.guru.langs.indexOf(lang)>=0) return 'guru';
	else if(t13n.alphabets.gujr.langs.indexOf(lang)>=0) return 'gujr';
	else if(t13n.alphabets.orya.langs.indexOf(lang)>=0) return 'orya';
	else if(t13n.alphabets.taml.langs.indexOf(lang)>=0) return 'taml';
	else if(t13n.alphabets.telu.langs.indexOf(lang)>=0) return 'telu';
	else if(t13n.alphabets.knda.langs.indexOf(lang)>=0) return 'knda';
	else if(t13n.alphabets.mlym.langs.indexOf(lang)>=0) return 'mlym';
	else return null;
};

/*
	create mapping from devanagari to other lipis like bengali etc
*/

t13n.alphabets={};

// 	store name, start unicode value
// 	jump is adder to shift deva letters to beng or other letters. unicodes for indian languages have been designed
// 		smart where similar sounding letters have been positioned at same place from starting letter in all lipis
//	map is function which processes complex transliterations

t13n.alphabets.deva = {};
t13n.alphabets.deva.unicode=2304;
t13n.alphabets.deva.name='देवनागरी, संस्कृत, हिन्दी, मराठी, नेपाली, भोजपुरी';
t13n.alphabets.deva.langs = ['hi', 'mr', 'sa', 'bh', 'ne'];

//from deva to beng
t13n.alphabets.deva.beng = {};
t13n.alphabets.deva.beng.diff=128;
t13n.alphabets.deva.beng.map=function(c){
	switch(c){
		case '।': return ' ';	//deva danda to latin blank space
		case '॥': return ' ';	//deva double danda to latin blank space
		case 'व': return 'ব';	//deva va to beng ba 
		case 'ॐ': return 'ॐ';	//deva om
		default: 
			var charCode = c.charCodeAt();		
			if(charCode<t13n.alphabets.deva.unicode || charCode>(t13n.alphabets.deva.unicode+128)){
				return c;
			}
			else{
				return String.fromCharCode(charCode+t13n.alphabets.deva.beng.diff);
			}
	}
}

//	from beng to deva

t13n.alphabets.beng = {};
t13n.alphabets.beng.unicode=2432;
t13n.alphabets.beng.name='বাংলা, অসমীয়া';
t13n.alphabets.beng.langs = ['bn', 'as' ]
t13n.alphabets.beng.deva = {};
t13n.alphabets.beng.deva.diff=-128;
t13n.alphabets.beng.deva.map=function(c){
	switch(c){
		default: 
			var charCode = c.charCodeAt();		
			if(charCode<t13n.alphabets.beng.unicode || charCode>(t13n.alphabets.beng.unicode+128)){
				return c;
			}
			else{
				return String.fromCharCode(charCode+t13n.alphabets.beng.deva.diff);
			}
	}
}

//	from deva to guru

t13n.alphabets.deva.guru = {};
t13n.alphabets.deva.guru.diff=256;
t13n.alphabets.deva.guru.map=function(c){
	switch(c){
		case '।': return ' ';	//deva danda to latin blank space
		case '॥': return ' ';	//deva double danda to latin blank space
		case 'ॐ': return 'ॐ';	//deva om
		case 'ऋ' : return 'ਰਿ';	//deva rri swar
		case 'ष' : return 'ਸ਼';	//deva murdhanya sha
		case 'ृ' : return '੍ਰਿ';	//deva rri swar with varna 
		case 'ऽ' : return 'ऽ';	//deva swar extension 
		default: 
			var charCode = c.charCodeAt();
			if(charCode<t13n.alphabets.deva.unicode || charCode>(t13n.alphabets.deva.unicode+128)){
				return c;
			}
			else{
				return String.fromCharCode(charCode+t13n.alphabets.deva.guru.diff);
			}
	}
}

//	from guru to deva

t13n.alphabets.guru = {};
t13n.alphabets.guru.unicode=2560;
t13n.alphabets.guru.name='ਪੰਜਾਬੀ';
t13n.alphabets.guru.langs = ['pa'];
t13n.alphabets.guru.deva = {};
t13n.alphabets.guru.deva.diff=-256;
t13n.alphabets.guru.deva.map=function(c){
	switch(c){
		default: 
			var charCode = c.charCodeAt();		
			if(charCode<t13n.alphabets.guru.unicode || charCode>(t13n.alphabets.guru.unicode+128)){
				return c;
			}
			else{
				return String.fromCharCode(charCode+t13n.alphabets.guru.deva.diff);
			}
	}
}

//	from deva to gujr

t13n.alphabets.deva.gujr = {};
t13n.alphabets.deva.gujr.diff=384;
t13n.alphabets.deva.gujr.map=function(c){
	switch(c){
	case '।': return ' ';	//deva danda to latin blank space
	case '॥': return ' ';	//deva double danda to latin blank space
	default: 
		var charCode = c.charCodeAt();		
		if(charCode<t13n.alphabets.deva.unicode || charCode>(t13n.alphabets.deva.unicode+128)){
			return c;
		}
		else{
			return String.fromCharCode(charCode+t13n.alphabets.deva.gujr.diff);
		}
	}
}

//	from gujr to deva

t13n.alphabets.gujr = {};
t13n.alphabets.gujr.unicode=2688;
t13n.alphabets.gujr.name='ગુજરાતી';
t13n.alphabets.gujr.langs = ['gu'];
t13n.alphabets.gujr.deva = {};
t13n.alphabets.gujr.deva.diff=-384;
t13n.alphabets.gujr.deva.map=function(c){
	switch(c){
		default: 
			var charCode = c.charCodeAt();		
			if(charCode<t13n.alphabets.gujr.unicode || charCode>(t13n.alphabets.gujr.unicode+128)){
				return c;
			}
			else{
				return String.fromCharCode(charCode+t13n.alphabets.gujr.deva.diff);
			}
	}
}

//	from deva to orya

t13n.alphabets.deva.orya = {};
t13n.alphabets.deva.orya.diff=512;
t13n.alphabets.deva.orya.map=function(c){
	switch(c){
	case '।': return ' ';	//deva danda to latin blank space
	case '॥': return ' ';	//deva double danda to latin blank space
	case 'ॐ': return 'ॐ';	//deva om
	default: 
		var charCode = c.charCodeAt();		
		if(charCode<t13n.alphabets.deva.unicode || charCode>(t13n.alphabets.deva.unicode+128)){
			return c;
		}
		else{
			return String.fromCharCode(charCode+t13n.alphabets.deva.orya.diff);
		}
	}
}

//	from orya to deva

t13n.alphabets.orya = {};
t13n.alphabets.orya.unicode=2816;
t13n.alphabets.orya.name='ଓଡ଼ିଆ';
t13n.alphabets.orya.langs = ['or'];
t13n.alphabets.orya.deva = {};
t13n.alphabets.orya.deva.diff=-512;
t13n.alphabets.orya.deva.map=function(c){
	switch(c){
		default: 
			var charCode = c.charCodeAt();		
			if(charCode<t13n.alphabets.orya.unicode || charCode>(t13n.alphabets.orya.unicode+128)){
				return c;
			}
			else{
				return String.fromCharCode(charCode+t13n.alphabets.orya.deva.diff);
			}
	}
}

//	from deva to taml

t13n.alphabets.deva.taml = {};
t13n.alphabets.deva.taml.diff=640;
t13n.alphabets.deva.taml.map=function(c){
	switch(c){
	case '।': return ' ';	//deva danda to latin blank space
	case '॥': return ' ';	//deva double danda to latin blank space
	case 'ऋ': return 'க்ரு';	//deva rhi to tamil combo rhi
	case 'ख': return 'க';	//deva kha to tamil ka
	case 'ग': return 'க';	//deva ga to tamil ka
	case 'घ': return 'க';	//deva gha to tamil ka
	case 'छ': return 'ச';	//deva chha to tamil cha
	case 'झ': return 'ஜ';	//deva jha to tamil ja
	case 'ठ': return 'ட';	//deva tha to tamil ta
	case 'ड': return 'ட';	//deva da to tamil ta
	case 'ढ': return 'ட';	//deva dha to tamil ta
	case 'थ': return 'த';	//deva Tha to tamil Ta
	case 'द': return 'த';	//deva Da to tamil Ta
	case 'ध': return 'த';	//deva Dha to tamil Ta
	case 'फ': return 'ப';	//deva pha to tamil pa
	case 'ब': return 'ப';	//deva ba to tamil pa
	case 'भ': return 'ப';	//deva bha to tamil pa
	default: 
		var charCode = c.charCodeAt();		
		if(charCode<t13n.alphabets.deva.unicode || charCode>(t13n.alphabets.deva.unicode+128)){
			return c;
		}
		else{
			return String.fromCharCode(charCode+t13n.alphabets.deva.taml.diff);
		}
	}
}

//	from taml to deva

t13n.alphabets.taml = {};
t13n.alphabets.taml.unicode=2944;
t13n.alphabets.taml.name='தமிழ்';
t13n.alphabets.taml.langs = ['ta'];
t13n.alphabets.taml.deva = {};
t13n.alphabets.taml.deva.diff=-640;
t13n.alphabets.taml.deva.map=function(c){
	switch(c){
		default: 
			var charCode = c.charCodeAt();		
			if(charCode<t13n.alphabets.taml.unicode || charCode>(t13n.alphabets.taml.unicode+128)){
				return c;
			}
			else{
				return String.fromCharCode(charCode+t13n.alphabets.taml.deva.diff);
			}
	}
}

//	from deva to telu

t13n.alphabets.deva.telu = {};
t13n.alphabets.deva.telu.diff=768;
t13n.alphabets.deva.telu.map=function(c){
	switch(c){
	case '।': return ' ';	//deva danda to latin blank space
	case '॥': return ' ';	//deva double danda to latin blank space
	case 'ॐ': return 'ॐ';	//deva om
	case '॰': return '॰';	//deva abbr sign
	default: 
		var charCode = c.charCodeAt();		
		if(charCode<t13n.alphabets.deva.unicode || charCode>(t13n.alphabets.deva.unicode+128)){
			return c;
		}
		else{
			return String.fromCharCode(charCode+t13n.alphabets.deva.telu.diff);
		}
	}
}

//	from telu to deva

t13n.alphabets.telu = {};
t13n.alphabets.telu.unicode=3072;
t13n.alphabets.telu.name='తెలుగు';
t13n.alphabets.telu.langs = ['te'];
t13n.alphabets.telu.deva = {};
t13n.alphabets.telu.deva.diff=-768;
t13n.alphabets.telu.deva.map=function(c){
	switch(c){
		default: 
			var charCode = c.charCodeAt();		
			if(charCode<t13n.alphabets.telu.unicode || charCode>(t13n.alphabets.telu.unicode+128)){
				return c;
			}
			else{
				return String.fromCharCode(charCode+t13n.alphabets.telu.deva.diff);
			}
	}
}

//	from deva to knda

t13n.alphabets.deva.knda = {};
t13n.alphabets.deva.knda.diff=896;
t13n.alphabets.deva.knda.map=function(c){
	switch(c){
	case '।': return ' ';	//deva danda to latin blank space
	case '॥': return ' ';	//deva double danda to latin blank space
	case 'ँ': return 'ಂ';	//chandra bindu by a bindu
	case 'ॉ': return 'ಾ';	// kannada equivalent of ा
	case 'ॐ': return 'ॐ';	//deva om
	case '॰': return '॰';	//deva abbr sign
	default: 
		var charCode = c.charCodeAt();		
		if(charCode<t13n.alphabets.deva.unicode || charCode>(t13n.alphabets.deva.unicode+128)){
			return c;
		}
		else{
			return String.fromCharCode(charCode+t13n.alphabets.deva.knda.diff);
		}
	}
}

//	from knda to deva

t13n.alphabets.knda = {};
t13n.alphabets.knda.unicode=3200;
t13n.alphabets.knda.name='ಕನ್ನಡ';
t13n.alphabets.knda.langs = ['kn'];
t13n.alphabets.knda.deva = {};
t13n.alphabets.knda.deva.diff=-896;
t13n.alphabets.knda.deva.map=function(c){
	switch(c){
		default: 
			var charCode = c.charCodeAt();		
			if(charCode<t13n.alphabets.knda.unicode || charCode>(t13n.alphabets.knda.unicode+128)){
				return c;
			}
			else{
				return String.fromCharCode(charCode+t13n.alphabets.knda.deva.diff);
			}
	}
}

//	from deva to mlym

t13n.alphabets.deva.mlym = {};
t13n.alphabets.deva.mlym.diff=1024;
t13n.alphabets.deva.mlym.map=function(c){
	switch(c){
	case '।': return ' ';	//deva danda to latin blank space
	case '॥': return ' ';	//deva double danda to latin blank space
	case 'ॐ': return 'ॐ';	//deva om
	default: 
		var charCode = c.charCodeAt();
		if(charCode<t13n.alphabets.deva.unicode || charCode>(t13n.alphabets.deva.unicode+128)){
			return c;
		}
		else{
			return String.fromCharCode(charCode+t13n.alphabets.deva.mlym.diff);
		}
	}
}

//	from mlym to deva

t13n.alphabets.mlym = {};
t13n.alphabets.mlym.unicode=3328;
t13n.alphabets.mlym.name='മലയാളം';
t13n.alphabets.mlym.langs = ['ml'];
t13n.alphabets.mlym.deva = {};
t13n.alphabets.mlym.deva.diff=-1024;
t13n.alphabets.mlym.deva.map=function(c){
	switch(c){
		default: 
			var charCode = c.charCodeAt();		
			if(charCode<t13n.alphabets.mlym.unicode || charCode>(t13n.alphabets.mlym.unicode+128)){
				return c;
			}
			else{
				return String.fromCharCode(charCode+t13n.alphabets.mlym.deva.diff);
			}
	}
}

//	transliterate function - ptext is given text, nscript is new lipi/lang, returns ntext in nscript
//	check for empty strings or invalid values
//	for each char, skip chars out of range of indian lang unicodes
//	find lang bucket the char falls, then pscript
//	if pscript and nscript are same, then return same char
//	if pscript is deva, then find transliterated char from map functions
//	else if nscript is deva, then find transliterated char from map functions
//	else find its deva equivalent and then its nscript text

t13n.t11e = function(nscript, ptext){
	var ntext = '';

	//return empty strings

	if(empty(nscript) || empty(ptext)){
		return ptext;
	}

	// check for valid nscript or lang
	if(t13n.scripts.list.indexOf(nscript)<0){
		nscript = t13n.scripts.getScriptByLang(nscript);
	}
	if(!nscript) return ptext;

	for ( var i = 0; i < ptext.length; i++) {
		var c = ptext.charCodeAt(i);

		if(c<t13n.scripts.unicode.start || c>t13n.scripts.unicode.end){
			ntext += String.fromCharCode(c);
		}

		else{
			var lipi_bucket = Math.floor((c-t13n.scripts.unicode.start)/128);
			var pscript = t13n.scripts.list[lipi_bucket];

			if(pscript == nscript){
				ntext += String.fromCharCode(c);
			}

			else if(pscript=='deva'){
				ntext += t13n.alphabets.deva[nscript].map(String.fromCharCode(c));
			}

			else if(nscript=='deva'){
				ntext += t13n.alphabets[pscript].deva.map(String.fromCharCode(c));
			}

			else{
				var c_deva = t13n.alphabets[pscript]['deva'].map(String.fromCharCode(c));
				ntext += t13n.alphabets.deva[nscript].map(c_deva);
			}
		}
	}

	return ntext;
}

//export module as nodejs app
try{
	if(module) module.exports = t13n;	
} catch(e){

}
