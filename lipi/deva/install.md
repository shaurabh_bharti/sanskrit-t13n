

#install m17n
sudo apt-get install ibus-m17n

dir of install
/usr/share/m17n/

#extension
http://www.nongnu.org/m17n/manual-en/m17nDBFormat.html#mdbIM

use right ALT+SHIFT for displaying lowercase roman characters
ALT+char is left as it may interfere with custom keyboard shortcuts (like Alt+f is for file menu on any window etc) 

folowing displays q when Alt+shift+q is pressed
((A-Q) ?q) 

special hindi characters like Ohm can be mapped to certain special characters are not mapped in inscript. 

#restart ibus daemon
restart to make changes take effect
	ibus restart 

#characters unmapped
॰ - abbreviation
० - hindi zero
ऽ - aalaap 

#char map
create a keyboard map for normal, shift, alt and alt+shift chars 

#customize ubuntu keyboard layout
https://help.ubuntu.com/community/Custom%20keyboard%20layout%20definitions?action=show&redirect=Howto%3A+Custom+keyboard+layout+definitions
http://askubuntu.com/questions/510024/what-are-the-steps-needed-to-create-new-keyboard-layout-on-ubuntu
http://askubuntu.com/questions/482678/how-to-add-a-new-keyboard-layout-custom-keyboard-layout-definition
https://help.ubuntu.com/community/ComposeKey
http://askubuntu.com/questions/482678/how-to-add-a-new-keyboard-layout-custom-keyboard-layout-definition

#sanskrit keyboard
this will have complete support for sanskrit keys and its common variations 
