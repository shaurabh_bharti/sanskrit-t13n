
var deva_text = 'बुद्धम्';
var kn_text = 'ಬುದ್ಧಮ್';
document.write(deva_text);
document.write('<br>');
document.write('<br>');
document.write(t13n.t11e('guru', kn_text));
document.write('<br>');


//test t13n
//t13n.lipi.suchi = ['deva', 'beng', 'guru', 'gujr', 'orya', 'taml', 'telu', 'knda', 'mlym'];
var sa_text = 'बुद्धम् शरणम् गच्छामि';
var beng_text = 'বুদ্ধম্ শরণম্ গচ্ছামি';
var guru_text = 'ਬੁਦ੍ਧਮ੍ ਸ਼ਰਣਮ੍ ਗਚ੍ਛਾਮਿ';
var gujr_text = 'બુદ્ધમ્ શરણમ્ ગચ્છામિ';
var orya_text = 'ବୁଦ୍ଧମ୍ ଶରଣମ୍ ଗଚ୍ଛାମି';
var taml_text = 'புத்தம் ஶரணம் கச்சாமி';
var telu_text = 'బుద్ధమ్ శరణమ్ గచ్ఛామి';
var knda_text = 'ಬುದ್ಧಮ್ ಶರಣಮ್ ಗಚ್ಛಾಮಿ';
var mlym_text = 'ബുദ്ധമ് ശരണമ് ഗച്ഛാമി';

document.write(t13n.t11e('beng', sa_text));
document.write('<br>');
document.write(t13n.t11e('deva', beng_text));
document.write('<br>');
document.write(t13n.t11e('guru', sa_text));
document.write('<br>');
document.write(t13n.t11e('deva', guru_text));
document.write('<br>');
document.write(t13n.t11e('gujr', sa_text));
document.write('<br>');
document.write(t13n.t11e('deva', gujr_text));
document.write('<br>');
document.write(t13n.t11e('orya', sa_text));
document.write('<br>');
document.write(t13n.t11e('deva', orya_text));
document.write('<br>');
document.write(t13n.t11e('taml', sa_text));
document.write('<br>');
document.write(t13n.t11e('deva', taml_text));
document.write('<br>');
document.write(t13n.t11e('telu', sa_text));
document.write('<br>');
document.write(t13n.t11e('deva', telu_text));
document.write('<br>');
document.write(t13n.t11e('knda', sa_text));
document.write('<br>');
document.write(t13n.t11e('deva', knda_text));
document.write('<br>');
document.write(t13n.t11e('mlym', sa_text));
document.write('<br>');
document.write(t13n.t11e('deva', mlym_text));
document.write('<br>');

/*function blank(x){
	if(empty(x)) return true;
	if(x.length<=0) return true;
	var i;
	for(i=0;i<x.length;i++){
		var c = x.charCodeAt(i);
		//control chars,32=blank,
		if(c<=32){
			continue;
		}
		else break;
	}
	if(i<x.length) return false;
	else return true;
}
*/


/*
t13n.t11e= function(plipi, nlipi, ptext) {

	var ntext = '';

	//return empty strings

	if (empty(ptext) || empty(plipi) || empty(nlipi)) {
		return ptext;
	}

	//return if plipi and nlipi values are not valid

	//else if()

	// return if both lipis are same

	else if (plipi == nlipi) {
		return ptext;
	}

	// if none of lipis are deva, 1st convert to deva and then convert to nlipi

	else if (plipi != 'deva' && nlipi != 'deva') {
		ntext = t13n.t11e(plipi, 'deva', ptext);
		ntext = t13n.t11e('deva', nlipi, ntext);
		return ntext;
	} else {
		//alert(t13n.varns[plipi][nlipi].jump);
		var jump = t13n.varns[plipi][nlipi].jump;
		var plipimin = t13n.varns[plipi].unicode;
		var plipimax = plipimin + 128;
		// for each char of ptext, shift it by map[plipi][nlipi] value stored by calling map fn
		for ( var i = 0; i < ptext.length; i++) {
			var c = ptext.charCodeAt(i);
			// if character is not part of plipi
			if (c < plipimin || c > plipimax) {
				ntext += String.fromCharCode(c);
				continue;
			} else {
				var customchar=t13n.varns[plipi][nlipi].map(String.fromCharCode(c));
				if(customchar){
					ntext+=customchar;
				} else {
					ntext += String.fromCharCode(c+jump);
				}
			}
		}
		//alert('ntext:' + ntext);
		return ntext;
	}
};
*/




/*
//plugin section scans for html tags, attributes which may contain sanskrit texts
var plugin = {};
/**
 * List each html tag and their attributes of and under node recursively, calls attributetext and tagtext for transliteration
 * @memberOf t13n.markup
 * @param {string} plipi Primary lipi
 * @param {string} nlipi New lipi
 * @param {Node} node Root XML node from where html elements are listed
 *  
 */

 /*
 
plugin.tags_attrs_t13n = function(plipi, nlipi, node) {
	var nodeName=node.nodeName.toLowerCase();
	//alert('node:'+nodeName);
	//text node
	if (node.nodeType==3){
		//alert('textnode,parentnode:'+node.parentNode.nodeName);
		plugin.tag_t13n(plipi, nlipi, node);
		return;
	}
	//attributes of element node
	else if(node.nodeType==1){
		//alert('attributes,node:'+nodeName);
		if(t13n.attributes[nodeName]){
			plugin.attr_t13n(plipi, nlipi, node, t13n.attributes[nodeName]);
		}
	}
	//node has children
	if(node.hasChildNodes()) {
		// find html tags of children
		var children = node.childNodes;
		//alert('node many:' + node.nodeName + ', count:' + children.length);
		for ( var i = 0; i < children.length; i++) {
			var child = children[i];
			// do not transliterate _lipilist select containing lipi list
			if (empty(child) || (child.nodeName.toLowerCase() == 'select' && child.className == 'lipioptions')) {
				continue;
			}
			//recursively list children tags of each child 
			else {
				plugin.tags_attrs_t13n(plipi, nlipi, child);
			}
		}
	}
	return;
};

*/

/**
 * Transliterates text of attributes of a tag node
 * @memberOf t13n.markup
 * @param {string} plipi Primary lipi
 * @param {string} nlipi New lipi
 * @param {Node} node Root XML node from where html elements are listed
 * @param {Object} attributes List of attributes for the node tag type
 */

 /*

t13n.attr_t13n= function(plipi, nlipi, node, attributes) {
	//alert('attributetext-node:' + node.nodeName + ', attribute:' + attributes);
	//make style related transliteration, like numbered lists
	//transliterate all attributes of node
	for(var i=0;i<attributes.length;i++){
		var attribute = attributes[i];
		if(node[attribute]){
			var ptext = node[attribute];
			if (!blank(ptext)) {
				var ntext = t13n.t11e(plipi, nlipi, ptext);
				node[attribute]= ntext;
			}
		}
	}
	return;
};

*/

/**
 * Transliterates text of a textnode of a tag
 * @memberOf t13n.markup
 * @param {string} plipi Primary lipi
 * @param {string} nlipi New lipi
 * @param {Node} node Root XML node from where html elements are listed
 */

 /*
t13n.tag_t13n= function(plipi, nlipi, node) {
	var ptext = node.nodeValue;
	//alert('tagtext-ptext:'+ptext + ', node:' + node.nodeName);
	if (!empty(ptext)) {
		node.nodeValue = t13n.t11e(plipi, nlipi, ptext);
	}
	return;
};

*/

/** 
 * List of attributes of html tags possibly containing text for translieration 
 * @memberOf t13n.markup
 * @type {Object} 
 */

 /*
t13n.attributes= {
	a: ['title','rel','rev'],
	b: ['title'],
	button: ['title','value'],
	form: ['title'],
	h1: ['title'],
	h2: ['title'],
	h3: ['title'],
	h4: ['title'],
	h5: ['title'],
	h6: ['title'],
	i: ['title'],
	img: ['title','alt'],
	input: ['title','alt','value','placeholder'],
	label: ['title'],
	li: ['title'],
	ol: ['title'],
	option: ['title','label','value'],
	select: ['title'],
	table: ['title','summary'],
	td: ['title','abbr'],
	textarea: ['title'],
	th: ['title','abbr'],
	tr: ['title'],
	u: ['title'],
	ul: ['title']
};

plugin.cookie = {};
plugin.cookie.create = function(name, value, days) {
	if (days) {
		var date = new Date();
		date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
		var expires = '; expires=' + date.toGMTString();
	} else
		var expires = '';
	document.cookie = name + '=' + value + expires + '; path=/';
};
plugin.cookie.read = function(name) {
	var nameEQ = name + '=';
	var ca = document.cookie.split(';');
	for ( var i = 0; i < ca.length; i++) {
		var c = ca[i];
		while (c.charAt(0) == ' ')
			c = c.substring(1, c.length);
		if (c.indexOf(nameEQ) == 0)
			return c.substring(nameEQ.length, c.length);
	}
	return null;
};
plugin.cookie.remove = function(name) {
	plugin.cookie.create(name, '', -1);
};

*/

//alert(t13n.attributes);

/**
 * Main transliterator for an html page
 * @memberOf _lipyak
 * @param {string} plipi Primary lipi
 * @param {string} nlipi New lipi
 */

 /*

_lipyak.lipyantarak=function(plipi, nlipi) {
	// transliterate title 
	if (_lipyak_opts.ttitle) {
		document.title = t13n.t11e(plipi, nlipi, document.title);
	}
	// for elements under document.body tag
	try {
		t13n.markup.htmltags(plipi, nlipi, document.body);
	} catch (err) {
		//alert('लिप्यांतरण में त्रुटि : ' + err.message);
	} finally {
		_lipyak_opts.clipi = nlipi;
	}
	return;
};

*/

//_lipyak.Page = {};

/**
 * Sets input variables if not set
 * @memberOf _lipyak.Page
 */

 /*

_lipyak.Page.setDefault= function(){
	//init vars if not set
	//check for correct values of each init vars
	if(empty(_lipyak_opts)){
		_lipyak_opts={};
	}
	//set primary lipi to devanagari, if not set
	if(empty(_lipyak_opts.plipi)){
		_lipyak_opts.plipi='deva';
	}
	if(empty(_lipyak_opts.clipi)){
		_lipyak_opts.clipi='deva';
	}
	if(empty(_lipyak_opts.ttitle)){
		_lipyak_opts.ttitle=true;
	}
	if(empty(_lipyak_opts.tclass)){
		_lipyak_opts.tclass=false;
	}
	if(empty(_lipyak_opts.lipioptionslist)){
		_lipyak_opts.lipioptionslist='deva,beng,guru,gujr,orya,taml,telu,knda,mlym';
	}
}

*/

/**
 * Called by select menu lipi list in html page, triggers transliterator
 * @memberOf _lipyak.Page
 * @param {event} e Html event Object
 */

 /*

_lipyak.Page.translipi = function (e) {
	//extract selected lipi value 
	var nlipi='';
	//IE fix
	if(navigator.appName == 'Microsoft Internet Explorer'){
		nlipi=window.event.srcElement.value;
	} else {
		nlipi=e.value;
	}
	if (nlipi == 'na') {
		plugin.cookie.remove('salipi');
		window.location.reload();
	}
	else {
		_lipyak.Page.setDefault();
		//set cookie for next 30 days
		plugin.cookie.create('salipi', nlipi, 30);
		//deva specific lipyantaran
		if(_lipyak_opts.clipi!=_lipyak_opts.plipi){
			window.location.reload();
		}
		
		var plipi=_lipyak_opts.plipi;
		// return if same lipi
		if (plipi == nlipi)
			return;
		//call the main transliterator fn
		_lipyak.lipyantarak(plipi, nlipi);
	}
}

*/

/**
 * Creates lipi list menu in html page for a list of lipis, sets nlipi as selected
 * @memberOf _lipyak.Page
 * @param {string} nlipi New lipi
 */

 /*
_lipyak.Page.createLipiMenu= function(nlipi){
	var selectslist = document.getElementsByTagName('select');
	if(selectslist){
		for(var i=0;i<selectslist.length;i++){
			var lipiselect = selectslist[i];
			var classname=lipiselect.className;
			if(!empty(classname) && classname=='lipioptions'){
				//add onchange listener
				if (window.addEventListener) {	//std
					lipiselect.addEventListener('change', function() {
						_lipyak.Page.translipi(this);
					}, false);
				} else if (window.attachEvent) {	//ie
					lipiselect.attachEvent('onchange', function() {
						_lipyak.Page.translipi(this);
					});
				} else {	//dep
					lipiselect.onchange = function() {	
						_lipyak.Page.translipi(this);
					};
				}
				//default option is set to NA
				var optelem = new Option('Select a New Language', 'na');
				//IE fix
				if(navigator.appName == 'Microsoft Internet Explorer'){
					optelem.innerText = 'Select a New Language';
				}
				//select this option if no lipi is selected in cookie
				if(!empty(nlipi)){
					optelem.selected='true';
				}
				lipiselect.appendChild(optelem);
				//add lipi options list
				if(_lipyak_opts.lipioptionslist){
					var lipilist = _lipyak_opts.lipioptionslist.split(',');
					for(var j=0;j<lipilist.length;j++){
						if(t13n.varns[lipilist[j]]){
							var optelem = new Option(t13n.varns[lipilist[j]].name, lipilist[j]);
							//IE fix
							if(navigator.appName == 'Microsoft Internet Explorer'){
								optelem.innerText=t13n.varns[lipilist[j]].name;
							}
							//select language option set in cookie
							if(lipilist[j]==nlipi) {
								optelem.selected='true';
							}
							lipiselect.appendChild(optelem);
						}
					}
				}
			}
		}
	}
}

*/

/**
 * Triggers auto transliteration of page
 */


 /*
if(_lipyak_opts){
	_lipyak.Page.setDefault();
	var plipi=_lipyak_opts.plipi;
	//lipi stored in cookie, if any
	var nlipi = plugin.cookie.read('salipi');	
	if (!empty(nlipi)){
		// update cookie further by a month
		plugin.cookie.create('salipi', nlipi, 30);
	}
	//add event to body onload : translierate all body text
	if (window.addEventListener) {	//std
		window.addEventListener('load', function() {
			_lipyak.Page.createLipiMenu(nlipi);
			_lipyak.lipyantarak(plipi, nlipi);
		}, false);
	} else if (window.attachEvent) {	//ie
		window.attachEvent('onload', function() {
			_lipyak.Page.createLipiMenu(nlipi);
			_lipyak.lipyantarak(plipi, nlipi);
		});
	} else {
		window.onload = function() {	//dep
			_lipyak.Page.createLipiMenu(nlipi);
			_lipyak.lipyantarak(plipi, nlipi);
		};
	}
}

*/
